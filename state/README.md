Introduction
============

Symulujemy zachowanie transakcji płatności w PAAY.

Transakcja ma kilka stanów:

1. Pending - w trakcie

Każda nowa transakcja zaczyna żywot w stanie "pending" - jest to stan wyjściowy.
Transakcja w tym stanie może zostać "porzucona", "odrzocona" lub "zatwierdzona".

2. Abandoned - porzucona

Transakcja zostaje uznana za porzuconą po 3 minutach od jej utworzenia, gdy nie zostanie "potwierdzona" lub "odrzucona".

3. Declined - odrzucona

Transakcja zostaje odrzucona, gdy użytkownik świadomie stwierdzi, że nie chce za nią płacić.

4. Approved - potwierdzona

Transakcja jest potwierdzona, gdy użytkownik świadomie ją potwierdza.

State paths
===================

Transakcje mogą zmieniać swój stan w określony sposób, tj.

Pending -> Abandoned
Pending -> Approved
Pending -> Declined

Transakcje NIE MOGĄ zmieniać swojego stanu w inny sposób. W takim wypadku transakcja powinna rzucić wyjątek, np.

Anandoned -> Approved !Exception
Approved -> Declined !Exception
itd.

Challenge
=========

Wymodeluj klase Transaction tak, aby wykonywala powyższe zmiany stanów.

Zakładany interfejs:

```
    namespace ACME;

    class Transasction {
        /**
         * @return Transaction
         */
        public function approve();

        /**
         * @return Transaction
         */
        public function decline();

        /**
         * @return Transaction
         */
        public function abandone();

        /**
         * @return string
         */
        public function state();
    }
```