<?php

namespace spec\ACME;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TransactionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ACME\Transaction');
    }

    function let()
    {
        $this->state()->shouldBe('pending');
    }

    function it_can_be_abandoned()
    {
        $this->abandone()->shouldBe($this);
        $this->state()->shouldBe('abandoned');
    }

    function it_can_be_approved()
    {
        $this->approve()->shouldBe($this);
        $this->state()->shouldBe('approved');
    }

    function it_can_be_declined()
    {
        $this->decline()->shouldBe($this);
        $this->state()->shouldBe('declined');
    }

    function it_cant_be_abandoned_if_not_pending()
    {
        $this
            ->approve()
            ->shouldThrow('\Exception')->duringAbandone()
        ;
    }

    function it_cant_be_approved_if_not_pending()
    {
        $this
            ->decline()
            ->shouldThrow('\Exception')->duringApprove()
        ;
    }

    function it_cant_be_declined_if_not_pending()
    {
        $this
            ->approve()
            ->shouldThrow('\Exception')->duringDecline()
        ;
    }
}
