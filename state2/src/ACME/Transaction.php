<?php

namespace ACME;

class Transaction
{
    private $state = 'pending';

    public function state()
    {
        return $this->state;
    }

    public function abandone()
    {
        if ('pending' !== $this->state) {
            throw new \Exception();
        }
        $this->state = 'abandoned';

        return $this;
    }

    public function approve()
    {
        if ('pending' !== $this->state) {
            throw new \Exception();
        }
        $this->state = 'approved';

        return $this;
    }

    public function decline()
    {
        if ('pending' !== $this->state) {
            throw new \Exception();
        }
        $this->state = 'declined';

        return $this;
    }
}
