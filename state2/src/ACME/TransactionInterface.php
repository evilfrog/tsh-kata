<?php
namespace ACME;

interface TransasctionInterface
{
    /**
     * @return Transaction
     */
    public function approve();

    /**
     * @return Transaction
     */
    public function decline();

    /**
     * @return Transaction
     */
    public function abandone();

    /**
     * @return string
     */
    public function state();
}
